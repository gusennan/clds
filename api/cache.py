"""A limitless key-value in-memory cache"""

class Cache:
    """A dictionary-based cache. Since a dictionary is used as the backend, all
    key values for get and set methods must be hashable."""

    def __init__(self):
        """Default constructor for the class."""
        self.cache = {}

    def get(self, key):
        """Attempts to get value for key in cache.

        args:
            key - the key of the cache value.
        returns:
            the value for key, or None if it doesn't exist in the cache."""
        return self.cache.get(key)

    def set(self, key, value):
        """Sets the value for key in the cache.

        If a value for the key already exists in the cache, it overwrites it
        and returns the old value.  Otherwise, it adds a new entry and returns
        None.

        args:
            key - the key for the cache.
            value - the value to set the key to."""

        prev = None

        if key in self.cache:
            prev = self.cache[key]
        self.cache[key] = value

        return prev
