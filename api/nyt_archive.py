"""This file contains an API for interacting with the New York Times' Archive
API.  

https://developer.nytimes.com/archive_api.json

The public API can be used as follows:
    
    1) instantiate an ApiReader with a New York times developer API key
    2) call the fetch_records function with the reader with a daterange you're
    interested in, and an optional section to filter the results by.

The articles returned from fetch_records are in a dictionary, with ('pub_date',
'news_desk', 'headline', 'section_name', 'web_url') keys.  The web_url can be
used for things later like crawling the page.
"""
import time, requests, sys
import argparse, json, os, stat
from collections import Counter
from datetime import datetime
from dateutil import parser as dp
from dateutil.relativedelta import relativedelta
from dateutil.tz import *

class JsonSource:
    """An abstract base class for a json provider.  Two implementations include
    a FileReader and an ApiReader."""

    def get_json(self, year, month):
        """Returns filtered by the month and year of the datetime_to_get.

        args:
            year: the year to get
            month: A [1,12] month index.  
        returns:
            a dictionary parsed from a json object, in the form specified by
            the NYT archive API. See the static_json directory for an example.
        """
        raise NotImplementedError("Abstract base method called.  You should" 
                                "have implemented this")

class FileReader(JsonSource):
    """An implementation of a JsonSource that is initialized with a stream, so
    that local json files can be used as a source for the fetch_records function.
    """
    def __init__(self, stream):
        self.stream = stream

    def get_json(self, year, month):
        raw_string = self.stream.read()
        return json.loads(raw_string)

class ApiReader(JsonSource):
    """An implementation of JsonSource that calls NYT's online API. Its
    get_json function can result in an Exception if an error occurs while
    communicating with the online API.
    """
    def __init__(self, api_key):
        self.api_key = api_key

    def get_json(self, year, month):
        parameters = { 'api-key' : self.api_key }
        url = "https://api.nytimes.com/svc/archive/v1/{0}/{1}.json".format(
            year, month)

        r = requests.get(url, params = parameters)
        r.raise_for_status()
        return r.json()

def fetch_records(json_source, start_datetime, end_datetime, section=None):
    """Retrieves an array of article records. 

    args:
        json_source: A JsonSource implementation
        start_datetime: the lower bound of the records to fetch
        end_datetime: the (exclusive) upper bound of the records to fetch
        section (optional): A filter on a record's 'section_name'
    
    returns:
        a list of records.
    """
    year_month_date = start_datetime.replace(day=1)
    years_months = [(start_datetime.year, start_datetime.month)]
    one_month = relativedelta(months=1)
    while year_month_date + one_month < end_datetime:
        year_month_date = year_month_date + one_month
        years_months.append((year_month_date.year, year_month_date.month))

    docs = []
    for year,month in years_months:
        json_month = json_source.get_json(year, month)
        docs += json_month['response']['docs']

    def doc_type_test(doc):
        return doc['document_type'] == 'article'

    def section_test(doc):
        return section and doc['section_name'] == section

    def date_test(doc):
        pub_date = dp.parse(doc['pub_date'])
        return start_datetime <= pub_date < end_datetime

    docs = filter(doc_type_test, docs)
    docs = filter(section_test, docs)
    docs = filter(date_test, docs)

    articles = ({
        'pub_date': doc['pub_date'],
        'news_desk': doc['news_desk'],
        'headline': doc['headline'],
        'section_name': doc['section_name'],
        'web_url': doc['web_url'],

    } for doc in docs)

    return articles

def eprint(*args, **kwargs):
    """A helper function to print to standard error"""
    print(*args, file=sys.stderr, **kwargs)




# Below are functions that help when this module is not being used as an API
# but rather from the command line during testing.
def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='static file to read json from')
    parser.add_argument('-d', '--startdate', help='the start date fetch records for'
                        'formatted YEAR-MONTH-DAY like 2017-02-01')
    parser.add_argument('-e', '--enddate', help='the end date fetch records for'
                        'formatted YEAR-MONTH-DAY like 2017-02-01')
    parser.add_argument('-o', '--output', help='the file to write the output to')
    parser.add_argument('-s', '--section', help='the newspaper section to'
                        'filter by')
    args = parser.parse_args()

    mode = os.fstat(sys.stdin.fileno()).st_mode
    input_stream = None
    input_file = None
    
    if stat.S_ISFIFO(mode) or stat.S_ISREG(mode):
        input_stream = sys.stdin

    if args.input:
        if not (stat.S_ISFIFO(mode) or stat.S_ISREG(mode)):
            input_file = args.input
        else:
            eprint("ignoring -i flag because input coming from stdin")

    if args.startdate:
        try:
            startdate = datetime.strptime(args.date, '%Y-%m-%d')
        except ValueError as e:
            parser.error('Start date must be formatted as YYYY-mm-dd\n')
    else:
        startdate = datetime.now(tzinfo=tzlocal())

    if args.enddate:
        try:
            enddate = datetime.strptime(args.date, '%Y-%m-%d')
        except ValueError as e:
            parser.error('End date must be formatted as YYYY-mm-dd\n')
    else:
        enddate = datetime.now(tzinfo=tzlocal())

    output_file = getattr(args, 'output', None)
    section = getattr(args, 'section', None)

    return input_stream, input_file, startdate, enddate, output_file, section


def main(argv):
    in_stream, in_file, startdate, enddate, out_file, section = parse_args(argv)

    if in_file:
        try:
            in_stream = open(in_file, 'r')
        except OSError:
            eprint('cannot open', in_file)
            sys.exit(1)

    records = fetch_records(FileReader(in_stream) if in_stream else
                          ApiReader(os.getenv('SCRAPER_NYT_API_KEY')),
                         startdate, enddate, section=section)

    if in_file:
        in_stream.close()

    if out_file:
        try:
            out_stream = open(out_file, 'w')
            sys.stdout = out_stream
        except OSError:
            eprint('cannot open', out_file)
            sys.exit(1)

    print('num records:', len(records))
    for rec in records:
        print(rec['headline']['main'])
        print('\t\turl: ' , rec['web_url'])
        print('\t\tsection: ' , rec['section_name'])

    if out_file:
        out_stream.close()

if __name__ == "__main__":
    sys.exit(main(sys.argv))
