"""Programmatic access to the microsoft translate API."""
import logging
import hashlib
import xml.etree.ElementTree as ET

import requests

class TextTooLongError(Exception):
    """Error that indicates the text is too long."""
    pass

_LOGGER = logging.getLogger(__name__)
_TRANSLATE_URL = 'https://api.microsofttranslator.com/v2/http.svc/Translate'

class Client:
    """A client for the Microsoft Translate API."""

    def __init__(self, authorization_token):
        """Creates a new API class.

        Args:
            authorization_token - a temporary authorization token to access
            microsoft's cognitive services."""
        self.token = authorization_token

    def translate_array(self, text_lst, to_lang, from_lang=None):
        """Uses the cognitive services translateArray endpoint"""

        m = hashlib.sha256()
        m.update(''.join(text_lst).encode('utf8'))
        digest = m.hexdigest()
        headers = {
            'Authorization': 'Bearer ' + self.token,
            'Accept': 'application/xml',
            'ContentType': 'text/xml', 
            'Request-ID': digest
        }
        print('digest:', digest)

        body = ET.Element('TranslateArrayRequest')
        from_node = ET.SubElement(body, 'From')
        from_node.text = from_lang or 'zh'
        to_node = ET.SubElement(body, 'To')
        to_node.text = to_lang
        texts_node = ET.SubElement(body, 'Texts')
        for text in text_lst:
            text_node = ET.SubElement(texts_node, 'string')
            text_node.text = text

        xml_str = ET.tostring(body, encoding="unicode")
        _LOGGER.warning('request')
        _LOGGER.warning(xml_str)

        resp = requests.post(_TRANSLATE_URL + 'Array', headers=headers,
                             data=xml_str.encode('utf-8'))
        resp.raise_for_status()
        _LOGGER.warning('response')
        _LOGGER.warning(resp.text)

## <string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">Trump</string>
        root_element = ET.fromstring(resp.text)
        return ET.ElementTree(element=root_element)


    def translate(self, text, to_lang, from_lang=None,
                  content_type='text/plain'):
        """Calls the translate endpoint to translate the text.

        Args:
            text - A string representing the text to translate. The size of the
            text must not exceed ten thousand characters.
            from_lang - A string representing the language code of the
            translation text. en = english, de = german etc...
            to_lang - A string representing the language code to translate the
            text into.
            content_type - The format of the text being translated. The
            supported formats are "text/plain" and "text/html". Any HTML needs
            to be well-formed.
        Returns:
            A string for the translated text."""

        params = {
            'text': text,
            'to':to_lang
        }

        if from_lang:
            params['from'] = from_lang
        if content_type:
            params['contentType'] = content_type

        headers = {
            'Authorization': 'Bearer ' + self.token,
            'Accept': 'application/xml'
        }

        resp = requests.get(_TRANSLATE_URL, params=params, headers=headers)
        resp.raise_for_status()

# <string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">Trump</string>
        root = ET.fromstring(resp.text)
        return root.text
