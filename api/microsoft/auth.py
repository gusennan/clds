"""Provides access to synchroniously fetch tokens from the Microsoft
Authentication Token API."""
import requests

_AUTH_URL = 'https://api.cognitive.microsoft.com/sts/v1.0/issueToken'

def get_auth_token(sub_key):
    """Fetch an authorization token for congnitive services.

    After 10 minutses, the authorization token is no longer valid and a new one
    must be requested.

    Args:
        sub_key - a subscription key for auth services.

    Returns:
        a base64-encoded string authorization token."""

    headers = {'Ocp-Apim-Subscription-Key': sub_key}
    resp = requests.post(_AUTH_URL, headers=headers)
    resp.raise_for_status()
    return resp.text
