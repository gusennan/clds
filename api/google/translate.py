"""Google Translate API Client"""
from googleapiclient.discovery import build

class TranslateClient: #pylint: disable=too-few-public-methods
    """Client for using Google to translate text."""

    def __init__(self, key):
        self.service = build('translate', 'v2', developerKey=key)

    def translate(self, texts, to_lang, from_lang=None):
        """Translates a list of texts."""
        size = 128
        results = []
        texts = list(texts)

        while len(texts) > 0:
            seg = texts[:size]
            texts = texts[min(size, len(texts)):]
            translations = self.service.translations() #pylint: disable=no-member
            query = None
            if from_lang:
                query = translations.list(
                    source=from_lang,
                    target=to_lang,
                    q=seg
                )
            else:
                query = translations.list(
                    target=to_lang,
                    q=seg
                )
            results += [translation['translatedText'] for translation in
                        query.execute()['translations']]


        return results
