"""Test module."""
from concurrent import futures
from itertools import zip_longest
import xml.etree.ElementTree as ET
import os
import sys

from .microsoft.auth import get_auth_token
from .microsoft.translate import Client

_MAX_WORKERS = 30

def main():
    """main function for the package."""
    subscription_id = os.environ["COGNITIVE_SERVICES_SUBSCRIPTION_KEY"]
    auth_token = get_auth_token(subscription_id)
    client = Client(auth_token)
    tokens = """北京 中国 核心勃勃 的 改革 经济 计划 陷入 了 困境 国企 效益 不 佳 对 它们
    进行 商业化 的 努力 遭到 挫败 视 众多 生活 在 城市 的 农民工 如同 二等 公民
    的 规定 也 几乎 没有 什么 松动"""
    tokens = tokens.split()
    to_do_map = {}

#    with futures.ThreadPoolExecutor(max_workers=_MAX_WORKERS) as executor:
#        zipper = zip_longest(tokens, [], fillvalue='en')
#        for tup in zipper:
#            future = executor.submit(client.translate, *tup)
#            to_do_map[future] = tup[0]
#        msg = "{source} is translated as {dest}"
#        to_do_iter = futures.as_completed(to_do_map)
#        for future in to_do_iter:
#            print(msg.format(source=to_do_map[future], dest=future.result()))
#

    translations = client.translate_array(tokens, 'en')

if __name__ == "__main__":
    sys.exit(main())
