import sys, os, argparse, gc, json, resource
from collections import namedtuple, defaultdict
import nltk
from nltk.probability import FreqDist
from nltk.corpus import reuters
from rocchio_classification import words_into_vecs, cosine_sim
from rocchio_classification import make_unit_tf_idf_vec
from rocchio_classification import make_unit_centroid
from rocchio_classification import is_in_class

r_precision = 6 # to how many decimal points should floats be rounded?

class TrainedRocchioModel:
    """This class is a container for different aspects of the trained model.
        
       centroids - a dictionary with a key of category name, and value a tuple
       of vector, # documents in vector pairs

       N - The number of documents in the set

       df - A nltk.probability.FreqDist of token <-> number of documents
           containing token pairs

       reuters_docs - A named tuple containing the following keys: 
           fileid - The raw filename for the file
           vec - The unit-sized document vector
           fdist - A nltk.probability.FreqDist representing the document
           cats - The categories that the document belows to
    """
    def __init__(self, centroids, N, df, reuters_docs):
        self.centroids = centroids
        self.N = N
        self.df = df
        self.reuters_docs = reuters_docs


def write_debug_json_files(training_model, fileids):
    """Writes out files used in unit tests to the json_docs directory
        
       The unit tests use the raw document Frequency Distributions, the 
       Document Frequency frequency distribution, and the total number of 
       documents.
    """
    directory_name = 'json_docs'
    for i, fileid in enumerate(fileids):
        path = os.path.join(directory_name, (fileid + '.json').replace('/', '_'))
        with open(path, 'w') as f:
            f.write(json.dumps(training_model.reuters_docs[i].fdist))

    with open(os.path.join(directory_name, 'document_freq.json'), 'w') as f:
        f.write(json.dumps(training_model.df))

    with open(os.path.join(directory_name, 'N'), 'w') as f:
        f.write(str(training_model.N))


def show_test_corpus_statistics(trained_rocchio_model):
    """Prints statistics about a run over the training document set.
    """

    # Compute statistics about centroids and documents that belong to their
    # classes.
    _print_statistics_on_clusters_and_cluster_documents(trained_rocchio_model)

    # Compute statistics about centroids and documents that do not belong to
    # their classes.
    _print_statistics_on_clusters_and_non_cluster_documents(trained_rocchio_model)

    # Compute precision/recall statistics for the training collection in order
    # to choose good cosine_sim values.
    _print_precision_recall_statistics(trained_rocchio_model)


def train_rocchio_model(training_fileids):
    """Load the files pointed to by argument and uses them to create a model.

    Returns a TrainedRocchioModel instance.
    """
    unit_doc_vectors, raw_freq_dist_vectors, term_doc_freq_dist = words_into_vecs(
        [reuters.words(fileid) for fileid in training_fileids])

    categories = [set(reuters.categories(fileids=fileid)) for fileid in
                  training_fileids]

    ReutersDoc = namedtuple('ReutersDoc', ['fileid', 'vec', 'fdist', 'cats'])
    reuters_docs = [ReutersDoc(zipped[0], zipped[1], zipped[2], zipped[3]) for zipped in 
                    zip(training_fileids, unit_doc_vectors,
                        raw_freq_dist_vectors,  categories)]

    centroids = {}
    for cat in reuters.categories():
        class_vectors = [doc.vec for doc in reuters_docs if cat in doc.cats]
        centroids[cat] = (make_unit_centroid(class_vectors), len(class_vectors))

    return TrainedRocchioModel(centroids, len(training_fileids), 
                         term_doc_freq_dist, reuters_docs)


def parse_args(argv):
    """Takes the arguments passed to the program and parses them.

    The supported arguments are:
        --gen-data - which is used to indicate the json_docs directory should
        have debug output written to it.
        --show-test-corpus-statistics - which indicates that the user wants 
        to have tests run on the calculated centroids and statistics about 
        the test collection printed to standard out.
    """
    parser = argparse.ArgumentParser(description='Use Rocchio Classification on'
                                    'Reuters docset')
    parser.add_argument('-g', '--gen-data', help='Generate data for the unit tests', 
                        action='store_true')
    parser.add_argument('-s', '--show-test-corpus-statistics', help='Should print '
                        'statistics about centroids and their cosine_sim '
                        'measurement with test document vectors', 
                        action='store_true')
    return parser.parse_args()


def get_reuters_fileids():
    """Helper to get the training and test fileids, which are returned in a
    list.
    """
    training_fileids, test_fileids = set(), set()
    for fileid in reuters.fileids():
        if fileid.startswith("test"):
            test_fileids.add(fileid)
        else:
            training_fileids.add(fileid)

    return training_fileids, test_fileids


def main(argv):
    args = parse_args(argv)
    training_fileids, test_fileids = get_reuters_fileids()
    training_model = train_rocchio_model(training_fileids)

    if args.gen_data:
        write_debug_json_files(training_model, test_fileids)

    if args.show_test_corpus_statistics:
        show_test_corpus_statistics(training_model)
        


# Private, helper functions
def _print_statistics_on_clusters_and_cluster_documents(trained_rocchio_model):
    """ Print statistics helper, finds and prints...

    - the minimum similiarity between a vector representing a class
    and a document that is labeled as a member of that class.

    - the average cosine_sim for each of the centroids and the
    documents that are members of that centroid.
    """

    min_cosine_sim = 1
    avg_cosine_sim = defaultdict(float)
    for doc in trained_rocchio_model.reuters_docs:
        for cat in doc.cats:
            sim = cosine_sim(doc.vec, trained_rocchio_model.centroids[cat][0])
            min_cosine_sim = min(min_cosine_sim, sim)
            avg_cosine_sim[cat] += sim

    for cat, (val, cnt) in trained_rocchio_model.centroids.items():
        avg_cosine_sim[cat] /= cnt
    print('minimum cosine_sim between centroids and documents: ',
          round(min_cosine_sim, r_precision))
    print('average cosine_sim between centroids and docs in class: ')
    for k, v in avg_cosine_sim.items():
        print('class: {} - sim: {}'.format(k, round(v, r_precision)))


def _print_statistics_on_clusters_and_non_cluster_documents(trained_rocchio_model):
    """ Print statistics helper, finds and prints...

    - the maximum similiarity between a vector representing a class
    and documents that are not members of that class.

    - the average similiarty of each of the centroids and the
    documents that are NOT member of that class.
    """

    max_discosine_sim = 0
    avg_discosine_sim = defaultdict(float)
    keys = trained_rocchio_model.centroids.keys()
    for doc in trained_rocchio_model.reuters_docs:
        for cat in keys:
            if not cat in doc.cats:
                sim = cosine_sim(doc.vec, trained_rocchio_model.centroids[cat][0])
                max_discosine_sim = max(max_discosine_sim, sim)
                avg_discosine_sim[cat] += sim

    for cat, (val, cnt) in trained_rocchio_model.centroids.items():
        avg_discosine_sim[cat] /= (len(trained_rocchio_model.reuters_docs) - cnt)

    print('maximum cosine_sim between centroids and documents not in class: ',
          round(max_discosine_sim, r_precision))
    print('average cosine_sim between centroids and documents not in class:')
    for k, v in avg_discosine_sim.items():
        print('class: {} - sim: {}'.format(k, round(v, r_precision)))


def _print_precision_recall_statistics(trained_rocchio_model):
    """ Print statistics helper, calculates and prints...

    - the precision/recall metrics for different parameters of
    cosine_sim, in a range of [0.1, 1] with increments of 0.1.
    """
    true_positive, false_positive, true_negative, false_negative = 0, 0, 0, 0
    for min_sim in range(1, 10, 1):
        min_sim = min_sim / 10
        for cat in trained_rocchio_model.centroids.keys():
            for doc in trained_rocchio_model.reuters_docs:
                in_cls = is_in_class(trained_rocchio_model.centroids[cat][0], doc.vec, min_sim)

                if cat in doc.cats:
                    if in_cls:
                        true_positive += 1
                    else:
                        false_negative += 1
                else:
                    if in_cls:
                        false_positive += 1
                    else:
                        true_negative += 1

            precision = true_positive / (true_positive + false_positive)
            recall = true_positive / (true_positive + false_negative)

        print('With min_sim of {}, precision: {}, recall: {}'.format(
            min_sim, precision, recall))

if __name__ == "__main__":
    # Limit the amount of memory the process can use to 1GB
    rsrc = resource.RLIMIT_DATA
    soft, hard = resource.getrlimit(rsrc)
    resource.setrlimit(rsrc, ((1024 * 1024 * 1024), (1024 * 1024 * 1024)))
    sys.exit(main(sys.argv))
