""" This file contains randomalia that I use while reading through the NLTK
book
"""

import nltk

def unusual_words(text):
    text_vocab = set(w.lower() for w in text if w.isalpha())
    english_vocab = set(w.lower() for w in nltk.corpus.words.words())
    unusual = text_vocab - english_vocab
    return sorted(unusual)

def content_fraction(text):
    stopwords = nltk.corpus.stopwords.words('english')
    content = [w for w in text if w.lower() not in stopwords]
    return len(content) / len(text)

def stress(pron):
    return [char for phone in pron for char in phone if char.isdigit()]

def gender_features(word):
    return { 
            'last_letter': word[-1],
           }

def gender_features2(name):
    features = {}
    features["first_letter"] = name[0].lower()
    features["last_letter"] = name[-1].lower()
    for letter in 'abcdefghijklmnopqrstuvwxyz':
        features["count({})".format(letter)] = name.lower().count(letter)
        features["has({})".format(letter)] = letter in name.lower()

    return features

from nltk.corpus import movie_reviews
movie_review_words = nltk.FreqDist(w.lower() for w in movie_reviews.words())
movie_review_features = list(movie_review_words)[:2000]

def movie_review_feature_generator(document):
    document_words = set(document)
    features = {}
    for word in movie_review_features:
        features["contains({})".format(word)] = (word in document_words)
    return features
