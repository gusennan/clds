import unittest, math, nltk
import random, string, json, os
import rocchio_classification as rc
from rocchio_classification import make_unit_vec

def randomword(length):
    return ''.join(random.choice(string.ascii_lowercase) for i in range(length))

class RocchioTests(unittest.TestCase):

    @classmethod 
    def setUpClass(cls):
        cls._fdists = []
        cls._normalized_vecs = []
        for path in sorted(os.listdir('json_docs')):
            if 'N' in path:
                with open(os.path.join('json_docs', path), 'r') as f:
                    cls._N = int(f.read())
                continue

            if 'document_freq' in path:
                with open(os.path.join('json_docs', path), 'r') as f:
                    cls._df = json.loads(f.read())
                continue

            with open (os.path.join('json_docs', path), 'r') as f:
                fdist = json.loads(f.read())
                cls._fdists.append(fdist)
                normalized_vec = rc.make_unit_tf_idf_vec(fdist,
                                                                 RocchioTests._N,
                                                                 RocchioTests._df)
                cls._normalized_vecs.append(normalized_vec)

    def setUp(self):
        self.basic_vec = {
            'a': 1,
            'b': 1,
            'c': 1
        }
        self.SaS = {
            'affection':115,
            'jealous':10,
            'gossip':2
        }
        self.PaP = {
            'affection':58,
            'jealous':7,
            'gossip':0
        }
        self.WH = {
            'affection':20,
            'jealous':11,
            'gossip':6
        }

        self.train1 = (True, ['Chinese', 'Beijing', 'Chinese'])
        self.train2 = (True, ['Chinese', 'Chinese', 'Shanghai'])
        self.train3 = (True, ['Chinese', 'Macao'])
        self.train4 = (False, ['Tokyo', 'Japan', 'Chinese'])
        self.test1 = (True, ['Chinese', 'Chinese', 'Chinese', 'Tokyo', 'Japan'])

    def tearDown(self):
        pass

    def test_tf_idf_with_normalization(self):
        training_docs = [nltk.FreqDist(self.train1[1]),
                         nltk.FreqDist(self.train2[1]),
                         nltk.FreqDist(self.train3[1]),
                         nltk.FreqDist(self.train4[1])]
        
        tf_idf_vec = rc._make_tf_idf_vec(training_docs[3], len(training_docs),
                                        rc.make_df(training_docs))
        tf_idf_vec = rc.make_unit_vec(tf_idf_vec)
        self.assertEqual(0, tf_idf_vec.get("Chinese", 0))
        self.assertAlmostEqual(0.71, tf_idf_vec.get("Japan", 0), places=2)
        self.assertAlmostEqual(0.71, tf_idf_vec.get("Tokyo", 0), places=2)
        self.assertEqual(0, tf_idf_vec.get("Beijing", 0))
        self.assertEqual(0, tf_idf_vec.get("Shanghai", 0))

    def test_vec_magnitudegth(self):
        self.assertEqual(rc.vec_magnitude(self.basic_vec), math.sqrt(3))

    def test_make_unit_vec(self):
        length_normalized = rc.make_unit_vec(self.basic_vec)
        for var in 'abc':
            self.assertEqual(length_normalized[var], 1 / math.sqrt(3))

        SaS_normalized = rc.make_unit_vec(self.SaS)
        PaP_normalized = rc.make_unit_vec(self.PaP)
        WH_normalized = rc.make_unit_vec(self.WH)
        self.assertAlmostEqual(SaS_normalized['affection'], 0.996, places=3)
        self.assertAlmostEqual(SaS_normalized['jealous'], 0.087, places=3)
        self.assertAlmostEqual(SaS_normalized['gossip'], 0.017, places=3)

        self.assertAlmostEqual(PaP_normalized['affection'], 0.993, places=3)
        self.assertAlmostEqual(PaP_normalized['jealous'], 0.120, places=3)
        self.assertAlmostEqual(PaP_normalized['gossip'], 0, places=3)

        self.assertAlmostEqual(WH_normalized['affection'], 0.847, places=3)
        self.assertAlmostEqual(WH_normalized['jealous'], 0.466, places=3)
        self.assertAlmostEqual(WH_normalized['gossip'], 0.254, places=3)

    def test_cos_cosine_sim(self):
        length_normalized = rc.make_unit_vec(self.basic_vec)
        self.assertAlmostEqual(1, rc.cosine_sim(length_normalized,
                                                length_normalized))
        self.assertAlmostEqual(0.999, rc.cosine_sim(make_unit_vec(self.PaP),
                                                    make_unit_vec(self.SaS)), places=2)
        self.assertAlmostEqual(0.888, rc.cosine_sim(make_unit_vec(self.WH),
                                                    make_unit_vec(self.SaS)), places=2)

        d = {}
        for i in range(20000):
            d[randomword(10)] = random.uniform(0, 1)

        length_normalized = make_unit_vec(d)
        self.assertAlmostEqual(1, rc.cosine_sim(length_normalized,
                                                length_normalized))

    def test_centroid_calculation(self):
        vectors, _, _ = rc.words_into_vecs([self.train1[1], self.train2[1], self.train3[1]])
        centroid = rc._make_centroid(vectors)

        for place in ['Beijing', 'Shanghai', 'Macao']:
            self.assertAlmostEqual(1/3, centroid[place])
        self.assertAlmostEqual(0, centroid['Chinese'])

    def test_not_losing_information_from_normalization(self):
        for normalized_vec in RocchioTests._normalized_vecs:
            self.assertAlmostEqual(1, rc.vec_magnitude(normalized_vec))

    def test_cosine_sim_with_self_is_1(self):
        for norm in RocchioTests._normalized_vecs:
            self.assertAlmostEqual(1, rc.cosine_sim(norm, norm))

    def test_centroid_is_length_normalized(self):
        centroid = rc.make_unit_centroid(RocchioTests._normalized_vecs)
        self.assertAlmostEqual(1, rc.vec_magnitude(centroid))
