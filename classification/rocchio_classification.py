import nltk, math, resource
from nltk.probability import FreqDist

def is_in_class(centroid, vec, min_sim):
    """ Determines whether cosine similiarity between the centroid vector and
    vec exceeds min_sim.

    args:
        centroid - dictionary as term unit vector for centroid
        vec - dictionary as term unit vector for document
        min_sim - the amount of cosine_sim the two vectors must exceed.

    Returns True is the cosine similiarity is at least min_sim, False
    otherwise.
    """
    return  cosine_sim(centroid, vec) >= min_sim


def cosine_sim(vec1, vec2):
    """ Computes the cosine similiarity between two unit vectors in a range of
    [0, 1].
    
    args:
        vec1 - the first dictionary (term -> tf-idf) vector
        vec2 - the second dictionary (term -> tf-idf) vector 

    returns:
        a float in the range of [0,1].  Because many floating-point
        multiplications are taking place, the results of this function 
        will usually not be exactly 0 or 1, and thus should be compared to be
        nearly, not exactly, equal to any value of interest.
    """
    sim = 0
    for k, v in vec1.items():
        if k in vec2:
            sim += v * vec2[k]

    return sim

def tf(freq):
    """ Calculates the term frequency value, using sublinear, logarithm-based
    TF scaling.

    args: 
        freq - an int indicating the number of times a term appears in a document.

    returns:
        a term-frequency floating-point value.
    """
    try:
        return 1 + math.log10(freq)
    except ValueError:
        return 0

def idf(collection_size, doc_freq):
    """ Calculates the inverse document-frequency, using a logarithmically
    increasing function over the inverse document frequency.  Range is
    constrained to [0, log10(collection_size].

    args:
        collection_size: the number of documents in the entire collection.
        doc_frequency: a term->no. of documents that term appears in
        dictionary.

    returns:
        an inverse document-frequency value

    """
    try:
        return math.log10(collection_size / doc_freq)
    except ZeroDivisionError:
        return 0

def vec_magnitude(vector):
    """ Calculates the magnitude of a vector.

    args:
        vector: a term->value dictionary.

    returns:
        the magnitude of the vector calculated by the Pythagorean formula.
    """
    series = 0
    for v in vector.values():
        series += v ** 2

    return math.sqrt(series)

def make_unit_vec(vector):
    """Creates a new unit vector pointing in the same direction as vector.

    args:
        vector: a term->value dictionary

    returns:
        a unit vector
    """
    copy = {}
    length = vec_magnitude(vector)
    for k, v in vector.items():
        copy[k] = v / length

    return copy

def make_df(freq_dist_list):
    """Creates a DF stucture, mapping terms to the number of
    documents that contain those terms. 

    args:
        freq_dist_list: a list of nltk.probability.FreqDist

    returns:
        an nltk.probability.FreqDist mapping a term to the number of documents
        it appears in.
    """
    return FreqDist([key for vec in freq_dist_list for key in vec.keys()])

def make_unit_tf_idf_vec(fdist, N, df_fdist):
    """Creates a unit vector from a document's term frequency distribution.

    args:
        fdist: a nltk.probability.FreqDist mapping term to number of occurences
            in a document.
        N: the total number of documents in the collection.
        df_fdist: a nltk.probability.FreqDist mapping a term to the number of
            documents it appears in.

    returns:
        A unit-length term->value dictionary
    """
    tf_idf_doc = _make_tf_idf_vec(fdist, N, df_fdist) 
    return make_unit_vec(tf_idf_doc)

def make_unit_centroid(vectors):
    """Takes a list of document vectors and calculates the average vector,
    representing a class centroid. 

    The returned centroid is length-normalized.

    args:
        vectors: A list of vectors in a class.

    returns:
        A new dictionary representing the class centroid.
    """
    unnormalized_centroid = _make_centroid(vectors)
    return make_unit_vec(unnormalized_centroid)

def words_into_vecs(list_of_docs):
    """Takes a list of tokenized documents and transforms them into vector
    space.

    args:
        a list of tokenized documents.

    returns:
        a list of vectors, in the same order as the function input, each
            representing a single document.  The vectors are unit vectors.
        a list of document -> term nltk.probability.FreqDist.  This list is 
            in the same order as the function input.
        a document frequency dict.  This dictionary's keys are terms and the
            values the number of documents that contain the term.
    """
    frequency_distributions = [nltk.FreqDist(doc) for doc in list_of_docs]
    document_frequency = make_df(frequency_distributions)
    collection_size = len(list_of_docs)
    unit_doc_vectors = [make_unit_tf_idf_vec(vec, collection_size,
                                             document_frequency)
                        for vec in frequency_distributions]

    return unit_doc_vectors, frequency_distributions, document_frequency 


# Private, helper functions
def _make_tf_idf_vec(bag_of_words_vec, collection_size, doc_frequency_dict):
        vec = {}

        for k, v in bag_of_words_vec.items():
            vec[k] = tf(v) * idf(collection_size,
                                            doc_frequency_dict[k])

        return vec


def _make_centroid(vectors):
    center = {}
    for vec in vectors:
        for word in vec:
            if word in center:
                center[word] += vec[word]
            else:
                center[word] = vec[word]

    num_vectors = len(vectors)
    for k, v in center.items():
        center[k] = v / num_vectors

    return center

