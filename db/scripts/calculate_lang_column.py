"""Script that adds the lang value for each row in the article table that needs
it."""
import argparse
from collections import namedtuple
import sys

from langdetect import detect
from langdetect.detector import LangDetectException

from db.articles_db import Connection
from pipeline.supported_lang import SupportedLang

def make_connection(host, port, database, user, password):
    """Creates a configured connection object."""
    settings_tup = namedtuple('Settings', 'db_name user password host port')
    settings = settings_tup(database, user, password, host, port)
    return Connection.setup(settings)

def update_lang(cursor, primary_key, lang):
    """Updates the language on the article row with id of pk."""
    query = """
        UPDATE article
        SET lang=%(lang)s
        WHERE id=%(pk)s;"""
    cursor.execute(query, {'lang': lang.value, 'pk': primary_key})

def delete_rows(connection, rows):
    """Deletes each row in rows."""

    query = """
        DELETE FROM article
        WHERE id = ANY(%(ids)s);"""
    with connection as conn:
        conn.cur.execute(query, {'ids': rows})

def update_rows(connection, rows):
    """Update the language of each row in rows."""
    with connection as conn:
        for row in rows:
            update_lang(conn.cur, row[0], row[1])


def run_program(connection, dry_run):
    """Runs the main logic of the program."""
    to_delete = []
    to_update = []
    with connection as conn:
        conn.cur.execute("SELECT id, content FROM article WHERE lang IS NULL;")
        rows = conn.cur.fetchall()
        for row in rows:
            try:
                lang = SupportedLang(detect(row[1]))
            except (LangDetectException, ValueError):
                to_delete.append(row[0])
            else:
                to_update.append((row[0], lang))

    if not dry_run:
        delete_rows(connection, to_delete)
        update_rows(connection, to_update)

    return to_delete, to_update


def main():
    """Main function for the module"""
    parser = argparse.ArgumentParser(description="Add lang column values to "\
                                     "article table")
    parser.add_argument('host', help='the host to connect to')
    parser.add_argument('port', help='the port to connect to')
    parser.add_argument('database', help='the database to connect to')
    parser.add_argument('user', help='the username to connect with')
    parser.add_argument('password', help='the user\'s password')
    parser.add_argument('--dry-run', help='don\'t save changes to DB',
                        action='store_true')
    args = parser.parse_args()

    connection = make_connection(args.host, args.port, args.database,
                                 args.user, args.password)

    deleted, updated = run_program(connection, args.dry_run)
    print(10 * '=', 'DELETED', 10 * '=')
    for gone in deleted:
        print(gone, end=' ')
    print()

    print(10 * '=', 'UPDATED', 10 * '=')
    for new in updated:
        print(new[0], end=' ')
    print()


if __name__ == "__main__":
    sys.exit(main())
