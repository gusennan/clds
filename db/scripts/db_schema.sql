DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS news_source;

CREATE TABLE news_source (
	id serial PRIMARY KEY,
	name varchar(128) UNIQUE NOT NULL,
	created_on timestamp without time zone default (now() at time zone 'utc') 
);

CREATE TABLE article (
	id serial PRIMARY KEY,
	headline text NOT NULL,
	content text NOT NULL,
	pub_date timestamp without time zone NOT NULL,
    web_url varchar(1024) NULL,
	news_source serial references news_source(id)
);

INSERT INTO news_source (name) VALUES ('nytimes');

INSERT 	INTO article 
		(headline, content, pub_date, news_source, web_url) 
	VALUES 
		('This is a dummy headline', 'On december 29th, the funniest thing happened...', now() at time zone 'utc', 1, 'http://no.news.com');
