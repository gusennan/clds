BEGIN;

ALTER TABLE news_source
RENAME COLUMN name TO domain;

ALTER TABLE news_source
ADD COLUMN name VARCHAR(128);

UPDATE news_source
SET name = 'New York Times Chinese'
WHERE id = 2;

UPDATE news_source
SET name = 'New York Times Style Chinese'
WHERE id = 3;

UPDATE news_source
SET name = 'New York Times'
WHERE id = 1;

ALTER TABLE news_source
ALTER COLUMN name SET NOT NULL;

COMMIT;
