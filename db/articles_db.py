"""This file provides an API for scrapy to interact with the database.

For the database connection to be successful, this module expects the following
attributes to be defined in the passed in settings object:

    - db_name
    - user
    - password
    - host
    - port

Then, the API is to be consumed as follows:

    1) Call the setup() class method with a settings instance.  This
        opens a connection to the database, which is maintained as a singleton
        in the class.

    2) When actions on the database need to be taken, call methods on this
        class.  The (only) correct way to initialize this class is using the
        "with Connection() as conn" context manager syntax.  This ensures
        a cursor is allocated for your methods and changes are committed.

    3) When finished, call teardown() to release the DB connection.
"""
import logging
from datetime import datetime
from collections import namedtuple

import psycopg2
import psycopg2.extras
from dateutil.tz import tzlocal

_LOGGER = logging.getLogger(__name__)

class Connection:
    """Represents a connection to the database."""

    def __init__(self):
        self.conn = None
        self.cur = None

    @classmethod
    def setup(cls, settings):
        """Initialize's a singleton connection to the database.

        Call the corresponding teardown method when done with the database.

            args:
                settings: a scrapy settings instance.
        """
        instance = cls()
        conn_str = "dbname='{0}' user='{1}' password='{2}' host='{3}' port='{4}'".format(
            settings.db_name,
            settings.user,
            settings.password,
            settings.host,
            settings.port)
        try:
            instance.conn = psycopg2.connect(conn_str)
        except psycopg2.Error:
            _LOGGER.error('shoot, could not connect to db')
            _LOGGER.error('used conn string: ' + conn_str)
            raise

        return instance


    def teardown(self):
        """Releases database resources held by the instance"""
        if self.conn:
            self.conn.close()

    def __enter__(self):
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.cur.close()

    def latest_article_date_for_source(self, source):
        """Gets the latest (aware) datetime for the source that is on record.

            args:
                source - the name of the source to query.  A string.

            returns:
                a datetime, defaulting to datetime min.
        """
        query = \
        """select max(pub_date)
            from article
            join news_source
                on article.news_source = news_source.id
            where news_source.domain = %(source)s;"""

        self.cur.execute(query, {'source': source})
        latest_date = self.cur.fetchone()
        return latest_date or datetime.min(tzlocal())

    def insert_article(self, article):
        """Adds a new article object to the article table.

            args:
                article - an ArticleItem instance.
        """
        insert_stmt = """
            INSERT INTO article
                (headline, content, pub_date, news_source, web_url, lang)
            VALUES
                (%(headline)s, %(content)s, %(pub_date)s, %(news_source)s,
                %(web_url)s, %(lang)s)"""

        self.cur.execute(insert_stmt, {
            'headline': article['headline'].strip(),
            'content': article['text'].strip(),
            'pub_date': article['pub_date'],
            'web_url': article['web_url'],
            'news_source': article['news_source'],
            'lang': article['lang']
        })

    def get_unprocessed_articles(self):
        """Retrieves all of the articles whose corenlp_json column is null.

        returns:
            a list of tuples (id, headline, text, news_source)
        """
        named_row = namedtuple('UnprocessedArticle',
                               'id headline content news_source lang')

        query = \
        """ SELECT id, headline, content, news_source, lang
            FROM article
            WHERE corenlp_json IS NULL """

        self.cur.execute(query)
        return (named_row(*row) for row in self.cur.fetchall())

    def get_untranslated_articles(self):
        """Fetches all of the articles whose words haven't yet been
        translated.

        Returns:
            A list of (id, corenlp_json) tuples"""

        result_tup = namedtuple('UntranslatedRow', 'id corenlp_json lang')
        query = \
                """ SELECT id, corenlp_json, lang
                    FROM article
                    WHERE NOT corenlp_json ? 'translated_on'
                    AND lang <> 'en'; """

        self.cur.execute(query)
        return [result_tup(row[0], row[1], row[2]) for row in self.cur.fetchall()]

    def set_corenlp_json_col(self, article_id, corenlp_json):
        """Updates the corenlp_json column of the article table.

        args:
            article_id - the id of the article to update.
            corenlp_json - a string in json format
        """

        update_stmt = \
        """ UPDATE article
            SET corenlp_json = %(corenlp_json)s
            WHERE id = %(article_id)s;"""

        self.cur.execute(update_stmt, {
            'corenlp_json': corenlp_json,
            'article_id': article_id
        })

    def get_corenlp_json_col(self, *article_ids):
        """Fetches the corenlp_json column for each id.

        args:
            *article_ids - the ids of the articles to get the corenlp_json
            column for.  If an ID is invalid (doesn't exist, or corenlp_json
            NULL, for example), that record is not returned.


        returns:
            a collection of (id, corenlp_json) tuples."""
        query = \
                """ SELECT id, corenlp_json
                    FROM article
                    WHERE id = ANY(%(article_ids)s);"""

        self.cur.execute(query, {'article_ids': list(article_ids)})
        results = self.cur.fetchall()
        return [res for res in results if res.corenlp_json is not None]
