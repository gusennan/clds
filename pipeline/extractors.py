"""Feature extractors that extract features from a CoreNLP annotated
response."""

def _make_response_extractor(resp_dict, key):
    """Creates an extractor from each token in a sentence."""
    for sentence in resp_dict['sentences']:
        for token in sentence['tokens']:
            yield token[key]


def words(article):
    """Extracts all the 'word' elements from the response."""
    return _make_response_extractor(article.corenlp_json, 'word')


def lemmas(article):
    """Extracts all the 'lemma' elements from the response."""
    return _make_response_extractor(article.corenlp_json, 'lemma')


def _pos_extractor(resp_dict, pos_set):
    """Helper function for extracting tokens that are a particular part of
    speech."""
    for sentence in resp_dict['sentences']:
        for token in sentence['tokens']:
            if token['pos'] in pos_set:
                yield token['word']

def nouns(article):
    """Extracts all the words that are nouns.

    The nouns are those tokens with the Tag 'NN', 'NNS',
    'NNP', and 'NNPS', standing for Noun, singluar or mass, Noun, plural,
    Proper noun, singular, Proper noun, plural for english articles.  For
    chinese articles, they are tagged with 'NT', 'NR', or 'NN' for Temporal
    Noun, Proper Noun, or Verbal Noun/Other Noun.

    See https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html
    and http://repository.upenn.edu/cgi/viewcontent.cgi?article=1039&context=ircs_reports
    """
    en_nouns_pos = set(('NN', 'NNS', 'NNP', 'NNPS'))
    ch_nouns_pos = set(('NT', 'NR', 'NN'))
    tags = en_nouns_pos if article.lang == 'en' else ch_nouns_pos
    return _pos_extractor(article.corenlp_json, tags)


def verbs(article):
    """Extracts all the words that are verbs.

    Verbs are those tokens with the tags 'VB', 'VBD', 'VBG', 'VBN', 'VBP', and
    'VBZ', standing for Verb base form, Verb past tense, Verb gerund or present
    participle, Verb past participle, Verb non-third person singular present,
    and Verb third-person singular present.

    For chinese the tags are VV, VA, VC, VE, for Other Verb, Predicative
    Adjective, Copula, and you3 as the main verb.
    """
    en_verb_pos = set(('VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ'))
    ch_verb_pos = set(('VV', 'VA', 'VC', 'VE'))
    tags = en_verb_pos if article.lang == 'en' else ch_verb_pos
    return _pos_extractor(article.corenlp_json, tags)

def ner_extractor(resp_dict, value):
    """Helper for different types of named-entities"""
    for sentence in resp_dict['sentences']:
        for token in sentence['tokens']:
            if token['ner'] == value:
                yield token['word']

def people(article):
    """Extract the named entities that are people."""
    return ner_extractor(article.corenlp_json, 'PERSON')


def locations(article):
    """Extract the named entities that are places."""
    return ner_extractor(article.corenlp_json, 'LOCATION')


def dates(article):
    """Extract the named entities that are dates."""
    return ner_extractor(article.corenlp_json, 'DATE')


def misc_ner(article):
    """Return misc. ners"""
    return ner_extractor(article.corenlp_json, 'MISC')
