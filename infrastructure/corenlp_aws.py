"""This module starts, manages, and stops a StanfordNLP server on AWS"""
import json
import logging
import os
import subprocess
import sys
import time

import boto3

_LOGGER = logging.getLogger(__name__)

# c4.large ~ $0.061 per Hour
# r4.large ~ $0.066 per Hour
_CORENLP_INSTANCE_TYPE = 'c4.large'
_SPOT_DEF_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                              'static_json')

class NLPServerAWS:
    """Models a StanfordNLP server running on a spot instance in AWS EC2."""

    def __init__(self, instance_type='c4.large'):
        """Initializes a new NLPServerAWS object.

        args:
            instance_type - type instance type to start.  There must be a
            corresponding json file with the name {instance_type}.json located
            in the static_json directory, as this function just loads that file
            as the spot instance request.
        """
        self.ip_addr = ''
        self.port = 80
        self.request_id = ''
        self.instance_id = ''
        self.instance_type = instance_type

    def start(self):
        """Spins up an EC2 spot instance for a fixed amount of time.

        After creating the instance, the unix utility netcat is used to wait
        until the Stanford NLP server is up and ready to field requests on the
        webserver.

        """
        with open(os.path.join(_SPOT_DEF_PATH,
                               '{0}.json'.format(self.instance_type))) as spot_fp:
            spot_req = json.loads(spot_fp.read())

        ec2 = boto3.resource('ec2')
        client = ec2.meta.client
        req_resp = client.request_spot_instances(**spot_req)
        request_id = req_resp['SpotInstanceRequests'][0]['SpotInstanceRequestId']

        status = ''
        while status != 'fulfilled':
            status_resp = client.describe_spot_instance_requests(
                SpotInstanceRequestIds=[request_id])
            status = status_resp['SpotInstanceRequests'][0]['Status']['Code']
            time.sleep(1)

        self.instance_id = status_resp['SpotInstanceRequests'][0]['InstanceId']
        self.request_id = request_id

        instances = ec2.instances.filter(InstanceIds=[self.instance_id])
        instance = list(instances)[0]

        # wait until server is up
        self.ip_addr = instance.public_ip_address
        while True:
            try:
                subprocess.check_call('nc -z {0} {1}'.format(self.ip_addr, self.port), shell=True)
                break
            except subprocess.CalledProcessError:
                time.sleep(1)

        return 'http://{0}:{1}'.format(self.ip_addr, self.port)

    def terminate(self):
        """Stops and terminates the EC2 spot instances."""
        ec2 = boto3.resource('ec2')
        client = ec2.meta.client

        client.cancel_spot_instance_requests(SpotInstanceRequestIds=[self.request_id])
        term_resp = ec2.instances.filter(InstanceIds=[self.instance_id]).terminate()

        resp_meta = term_resp[0]['ResponseMetadata']
        status_code = int(resp_meta['HTTPStatusCode'])
        _LOGGER.info('HTTP Response code from terminate was %s', status_code)

        term_state = term_resp[0]["TerminatingInstances"][0]
        current_state = term_state['CurrentState']['Name']
        prev_state = term_state['PreviousState']['Name']
        _LOGGER.info('instance %s was in %s, now in %s',
                     self.instance_id, prev_state, current_state)



def print_status_msg(msg):
    """Helper function to print pretty status messages"""
    print(5*'*', msg, 5*'*')

def main():
    """Main function for the module.  Starts and stops an OpenNLP server."""
    print_status_msg('Starting.  Instance Type - {0}'.format(
        _CORENLP_INSTANCE_TYPE))
    server = NLPServerAWS(instance_type=_CORENLP_INSTANCE_TYPE)
    try:
        ip_addr = server.start()
    except OSError as err:
        print_status_msg('Error encountered while starting server')
        print(err)

    print_status_msg('Server started with IP: {0}'.format(ip_addr))
    print_status_msg('Stopping server')
    server.terminate()
    print_status_msg('Server stopped')


if __name__ == "__main__":
    sys.exit(main())
