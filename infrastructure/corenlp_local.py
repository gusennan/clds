"""This module allows programmatic access for starting and stopping a local
CoreNLP server."""
import logging
import subprocess
import os
from os import path
import sys
import tempfile
import time

import requests

_LOGGER = logging.getLogger(__name__)
_CORENLP_SRC_DIR = '{0}/bin/stanford-corenlp-full-2016-10-31'.format(os.environ['HOME'])

class NLPServerLocal:
    """Models a StanfordNLP server running on localhost."""

    def __init__(self, port='9000'):
        self.port = int(port)
        self.shutdown_key = ''
        self.ip_addr = '127.0.0.1'

    def start(self):
        """Starts a local instance of a StanfordNLP server.

        Waits for the server to start responding before returning from the
        function."""
        subprocess.call("java -Xmx4g -cp \"{0}/*\"" \
                        " edu.stanford.nlp.pipeline.StanfordCoreNLPServer" \
                        #                " -serverProperties StanfordCoreNLP-chinese.properties" \
                        " -port {1} -timeout  15000 &".format(_CORENLP_SRC_DIR, self.port),
                        shell=True)

        while True:
            try:
                subprocess.check_call('nc -z {0} {1}'.format(self.ip_addr, self.port), shell=True)
                break
            except subprocess.CalledProcessError:
                time.sleep(1)

        tmp_dir = tempfile.gettempdir()
        tmp_path = path.join(tmp_dir, 'corenlp.shutdown')
        with open(tmp_path, 'r') as shutdown_fp:
            self.shutdown_key = shutdown_fp.read()

        return 'http://{0}:{1}'.format(self.ip_addr, self.port)

    def terminate(self):
        """Shuts down the local CoreNLP server."""
        payload = {"key" : self.shutdown_key}
        url = 'http://{0}:{1}/shutdown'.format(self.ip_addr, self.port)
        response = requests.get(url, params=payload)
        response.raise_for_status()

def print_status_msg(msg):
    """Helper function to print pretty status messages"""
    print(5*'*', msg, 5*'*')

def main():
    """Main function for the module.  Starts and stops an OpenNLP server."""
    server = NLPServerLocal(port=8000)
    addr = server.start()
    print_status_msg('Server started with address: {0}'.format(addr))
    print_status_msg('Stopping server')
    server.terminate()
    print_status_msg('Server stopped')


if __name__ == "__main__":
    sys.exit(main())
