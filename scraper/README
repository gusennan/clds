=====================================
Cross Language New Article Clustering
=====================================

Scraper: This repository contains all of the source code necessary for obtaining
news articles for clustering.  There are currently two directories:

api/
	The python modules for interacting with online webservice APIs.
	Currently this is just the New York Time's archive service.

db/
	Contains the schema setup and other sql needed for initializing a
	database to support the news-article scraping. Also contains the python
	modules for interacting the database.


news_articles/
	Contains all of the python source code for fetching news articles from
	APIs or scraping them directly from news articles websites.  This
	directory contains a scrapy.cfg file, making it the root directory of
	a scrapy[1] project.


To setup your environment for running the scraper, you need to have access to a
postgres database (either on your local computer or somewhere else), and then
install the required development dependencies on your linux machine.  These
dependencies include (on ubuntu)

postgresql-client-common
postgresql-client
python3-venv
python3.4-venv
libpg-dev
python3-dev
libffi-dev
libxml2-dev
libxlt1-dev
libxslt1-dev

Afterwards, you should:

1) Make a python3 virtual environment to manage your runtime path
for this project.  To do this, follow the instructions on 
https://docs.python.org/3/tutorial/venv.html

2) Activate the virtual environment 

3) Install the python dependencies using pip:
	pip install -r requirements.txt

4) In order to save the articles into a database, the scraper expects a few
environment variables to be defined.  These expected environment variables are:
	(For all spiders)
	SCRAPER_POSTGRES_HOST - the IP address of the computer with the 
		postgres server
	SCRAPER_POSTGRES_USER - the user to user for connecting to the 
		postgres DB
	SCRAPER_POSTGRES_PASSWORD - the user's password
	SCRAPER_POSTGRES_DB_NAME - the name of the database in the server 
		to connect to
	SCRAPER_POSTGRES_PORT - the port on which the postgres server 
		is running

	(For the nyt spider)
	SCRAPER_NYT_API_KEY - an API key from the new york times.  See
		developer.nytimes.com for instructions on how to get one.
	SCRAPER_NYT_USERNAME - an account is needed to fetch articles from the
		new york times' webpages. This is the account username. 
	SCRAPER_NYT_PASSWORD - The password for the new york times account.

The currently available spiders are 'nyt' and 'cn_nyt'.  The 'nyt' spider, by
default, fetches all articles from the World section from the first of the month
until the present.  The 'cn_nyt' scraper scrapes all of the international
articles in Chinese and English for the same date range as the nyt scraper, from
the cn.nytimes.com and cn.nytstyle.com domains.

To run a spider, such as the NYT spider, cd into the news_articles directory
(the directory with the scrapy.cfg file) and execute:

$ scrapy crawl nyt

By default, the nyt spider logs into the New York Times, and crawls the first of
the month to yesterday for articles.  If yesterday was the last day of the
previous month, then the date range used is the first of the previous month to
yesterday. 

To manually modify the start and end dates of the scraper, you can use scrapy's
'-a' flag for the spider as so:

$ scrapy crawl nyt -a begin_date=20170101 -a end_date=20170301

Note that the dates are specified in YYYYmmdd format.

[1] https://scrapy.org/  To see an architectural overview of how scrapy is put
together, see https://doc.scrapy.org/en/1.3/topics/architecture.html
