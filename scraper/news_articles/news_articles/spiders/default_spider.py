"""DefaultSpider allowing for timespan of scraped articles to be specified on the
command line.

To specify the date range on the command line that you want to scrape, add the
begin_date and end_date attributes to this class through Scrapy's

scrapy crawl -a NAME=VALUE

syntax.

"""

from datetime import datetime, timedelta
from enum import Enum, unique

from scrapy import Spider
from dateutil import parser as dp
from dateutil.tz import tzlocal

class DefaultSpider(Spider):
    """Base class for spiders that scrape news articles."""

    def __init__(self):
        super().__init__()
        self.specified_date = False
        self.connection = None
        self.begin_date = getattr(self, 'begin_date', None)
        self.end_date = getattr(self, 'end_date', None)

    def parse(self, response):
        raise NotImplementedError

    def start_requests(self):
        now = datetime.now(tzlocal())
        one_day = timedelta(days=1)
        yesterday = now - one_day
        begin_date = yesterday.replace(day=1, hour=0, minute=0, second=0)
        end_date = yesterday

        if self.begin_date:
            self.specified_date = True
            begin_date = dp.parse(self.begin_date).replace(tzinfo=tzlocal())
        if self.end_date:
            self.specified_date = True
            end_date = dp.parse(self.end_date).replace(tzinfo=tzlocal())

        self.begin_date = begin_date
        self.end_date = end_date
