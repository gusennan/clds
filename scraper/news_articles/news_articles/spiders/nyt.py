"""A spider that logs into NYT and scrapes the world articles."""
from datetime import datetime

from scrapy import FormRequest
from scrapy.http import Request
from dateutil import parser as dp
from dateutil.tz import tzutc

from api import nyt_archive
from news_articles.items import ArticleItem
from pipeline.supported_lang import SupportedLang
from .default_spider import DefaultSpider

class NytSpider(DefaultSpider):
    """The main class that handles scraping the New York Times."""
    name = "nyt"
    allowed_domains = ["nytimes.com"]

    def __init__(self):
        super().__init__()
        self.last_fetch_date = None

    def start_requests(self):
        super().start_requests()

        # The connection attribute is set in the DBInitializer extension
        with self.connection as connection: # pylint: disable=not-context-manager
            self.last_fetch_date = connection.latest_article_date_for_source(
                'nytimes.com')
            self.last_fetch_date = self.last_fetch_date[0] or datetime.min.replace(tzinfo=tzutc())

        yield Request('https://myaccount.nytimes.com/auth/login',
                      self.parse)

    def parse(self, response):
        """Logs into the New York Times, which causes the HTTP client that
        scrapy uses to remember the authentication cookies necessary for
        scraping actual articles.
        """
        req = FormRequest.from_response(
            response,
            formdata={
                'userid': self.settings.get('NYT_LOGIN_USERNAME'),
                'password': self.settings.get('NYT_LOGIN_PASSWORD'),
                'password_text': self.settings.get('NYT_LOGIN_PASSWORD')
            },
            callback=self.crawl_articles
        )
        req.meta['handle_httpstatus_list'] = [301, 302, 303]
        return req

    def crawl_articles(self, response):
        """Fetches world articles that we haven't already downloaded.

        First checks the datetime of the latest article downloaded from
        nytimes.com and only gets those articles that are at a greater date
        than those articles.
        """
        if response.status != 302:
            self.logger.error('Login failed')
            return

        records = nyt_archive.fetch_records(
            nyt_archive.ApiReader(self.settings.get('NYT_API_KEY')),
            self.begin_date, self.end_date,
            'World')

        if not self.specified_date:
            records = [rec for rec in records if
                       dp.parse(rec['pub_date']) > self.last_fetch_date]

        for rec in records:
            req = Request(rec['web_url'], callback=self.fetched_world_page)
            req.meta['nyt_login_api_data'] = rec
            yield req

    def fetched_world_page(self, response):
        """Parses the HTML from an article in the 'World' section and yields
        ArticleItems."""
        self.logger.info('NYT Page Response status: {0}'.format(response.status))

        paragraphs = response.css('p.story-body-text')
        text = ' '.join(list(''.join(p.css('::text').extract()) for p in
                             paragraphs))
        api_data = response.request.meta['nyt_login_api_data']

        yield ArticleItem(pub_date=api_data['pub_date'],
                          headline=api_data['headline']['main'],
                          web_url=api_data['web_url'],
                          text=text,
                          section_name=api_data['section_name'],
                          lang=SupportedLang.ENGLISH.value,
                          news_source=1)
