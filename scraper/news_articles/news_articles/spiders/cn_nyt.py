"""Scraper for cn.nytimes.com and cn.nytstyle.com. Handles downloading all of
the articles in the world section off of those two sites."""
from datetime import datetime
import re
from urllib.parse import urlparse

from scrapy import Request
from scrapy.linkextractors import LinkExtractor
from dateutil import parser as dp
from dateutil.relativedelta import relativedelta
from dateutil.tz import tzlocal
from langdetect import detect
from langdetect.detector import LangDetectException

from news_articles.items import ArticleItem
from pipeline.supported_lang import SupportedLang
from .default_spider import DefaultSpider

class CnNytSpider(DefaultSpider):
    """The spider that controls parsing of cn.nytimes.com and
    cn.nytstyle.com"""
    name = "cn_nyt"
    allowed_domains = ['cn.nytimes.com', 'cn.nytstyle.com']

    def __init__(self):
        super().__init__()
        self.last_dl_date = None
        self.story_extractor = None
        self.next_page_extractor = None

    def start_requests(self):
        """Signs into NYT and kicks off the scraping process."""
        super().start_requests()
        with self.connection as conn: # pylint: disable=not-context-manager
            self.last_dl_date = conn.latest_article_date_for_source('cn.nytimes.com')[0]
            self.last_dl_date = self.last_dl_date or self.begin_date - relativedelta(seconds=1)

        self.story_extractor = LinkExtractor(allow=[r'cn.nytstyle.com', r'cn.nytimes.com'],
                                             restrict_css=['.sectionLeadHeader', '.autoListStory'])
        self.next_page_extractor = LinkExtractor(
            allow=r'cn.nytstyle.com|cn.nytimes.com',
            restrict_css=['.next'])

        yield Request('http://cn.nytimes.com/world/', self.parse)

    def parse(self, response):
        stories = self.story_extractor.extract_links(response)
        next_page = self.next_page_extractor.extract_links(response)

        def extract_pubdate(link):
            """Extracts the publication date from a URL"""
            match = re.search(r'\d{8,8}', link.url)
            date_in_url = match.group()
            return dp.parse(date_in_url).replace(tzinfo=tzlocal())

        def story_in_range(link):
            """returns True if a URL's pubdate is in the scraping daterange."""
            pub_date = extract_pubdate(link)
            if self.specified_date:
                return self.begin_date <= pub_date < self.end_date
            else:
                return self.last_dl_date < pub_date < self.end_date

        yield from map(lambda link: Request(link.url, self.parse_article),
                       filter(story_in_range, stories))

        pub_dates = sorted(map(extract_pubdate, stories), reverse=True)

        if not pub_dates[-1] < self.begin_date:
            yield Request(next_page[0].url, self.parse)

    def parse_article(self, response):
        """Extracts the text from an article's webpage."""
        pub_date = datetime.min
        headline = response.css('.articleHeadline::text').extract_first()
        web_url = response.url
        possible_date = re.findall(r'\d{8,8}', web_url)
        parsed_url = urlparse(web_url)
        section_name = parsed_url.path.split('/')[1]
        paragraph_sep = '\n\n'
        if len(possible_date) > 0:
            pub_date = dp.parse(possible_date[0])

        text = paragraph_sep.join(response.css('.paragraph::text').extract())
        domain = 3 if parsed_url.netloc == 'cn.nytstyle.com' else 2

        try:
            detected_lang = SupportedLang(detect(text))
        except LangDetectException:
            return
        except ValueError:
            msg = "Parsed an article with unsupported language: {0}"
            self.logger.warning(msg.format(detected_lang))
            return
        else:
            yield ArticleItem(pub_date=pub_date,
                              headline=headline,
                              web_url=web_url,
                              text=text,
                              section_name=section_name,
                              lang=detected_lang.value,
                              news_source=domain)

        en_version = response.css('.en-us').xpath('./a/@href').extract_first()
        if detected_lang == SupportedLang.MAINLAND_CHINESE and en_version:
            en_url = web_url + 'en-us'
            yield Request(en_url, callback=self.parse_article)
