# -*- coding: utf-8 -*-
"""This file is for defining pipelines, or actions that are taken after a HTML
page is downloaded.  Currently, there is a PostgreSqlPipeline, which saves the
articled fetched from the nyt spider to a postgres database.
"""
class PostgreSqlPipeline(object):

    @classmethod
    def from_crawler(cls, crawler):
        return cls()

    def process_item(self, item, spider):
        with spider.connection as database:
            database.insert_article(item)
        return item
