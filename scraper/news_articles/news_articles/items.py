# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class ArticleItem(scrapy.Item):
    """A model for an Article record."""
    headline = scrapy.Field()
    text = scrapy.Field()
    pub_date = scrapy.Field()
    web_url = scrapy.Field()
    news_desk = scrapy.Field()
    section_name = scrapy.Field()
    lang = scrapy.Field()
    news_source = scrapy.Field()

