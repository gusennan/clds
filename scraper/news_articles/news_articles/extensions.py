"""Contains extensions for scrapy"""
from collections import namedtuple

from scrapy import signals
from scrapy.exceptions import NotConfigured

from db.articles_db import Connection

class DBProvider:
    """An extension that opens a connection to the database
    when a spider is opened and closes the connection when the
    spider is closed.

    Once the connection is opened, clients use the Connection class
    defined in the articles_db module for CRUD operations on the database.
    """
    @classmethod
    def from_crawler(cls, crawler):
        """Connects the spider_opened and spider_closed signals to functions in
        this class so that a database connection can be given to each
        spider."""

        if not crawler.settings.getbool('DBPROVIDER_ENABLED'):
            raise NotConfigured

        ext = cls()
        crawler.signals.connect(ext.spider_opened,
                                signal=signals.spider_opened)
        crawler.signals.connect(ext.spider_closed,
                                signal=signals.spider_closed)

        return ext

    def spider_opened(self, spider): #pylint: disable=no-self-use
        """Instantiates a connection and adds it into the spider's connection
        property."""
        settings = namedtuple('settings', 'db_name user password host port')
        spid_set = spider.settings
        config = settings(spid_set.get('POSTGRES_DB_NAME'),
                          spid_set.get('POSTGRES_USER'),
                          spid_set.get('POSTGRES_PASSWORD'),
                          spid_set.get('POSTGRES_HOST'),
                          spid_set.get('POSTGRES_PORT'))

        spider.connection = Connection.setup(config)


    def spider_closed(self, spider): #pylint: disable=no-self-use
        """This function releases the connection from the spider."""
        spider.connection.teardown()
        spider.connection = None
