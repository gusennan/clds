"""Script for grabbing a corenlp_json column"""
import json

import psycopg2

_QUERY = """
SELECT corenlp_json
FROM article
WHERE corenlp_json->>'translated_by' = 'google translate'
AND id = ANY(
	(SELECT id
	FROM article
	WHERE corenlp_json ? 'translated_on'
	AND corenlp_json ? 'translated_by' 
	AND lang = 'zh-cn'))
ORDER BY pub_date DESC
LIMIT 1;"""

def main():
    """Main function"""
    conn = psycopg2.connect('user=postgres dbname=articles')
    cur = conn.cursor()
    cur.execute(_QUERY)
    res = cur.fetchone()[0]

    with open('test.out', 'w') as file_pointer:
        file_pointer.write(json.dumps(res))

if __name__ == "__main__":
    main()
