SELECT count(id)
FROM article
WHERE processed_json->>'translated_by' = 'google translate'
AND id = ANY(
	(SELECT id
	FROM article
	WHERE processed_json ? 'translated_on'
	AND processed_json ? 'translated_by' 
	AND lang = 'zh-cn'));
